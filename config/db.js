const mongoose = require('mongoose');
const config = require('config');

const db = config.get('mongoURI');

const connectDB = async () => {

    try{
        await mongoose.connect(config.mongoURI, {useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true});
    } catch(e) {
        console.log("Error " + e.message);
        process.exit(1);
    }
    console.log("MongoDB connected");
}

module.exports = connectDB;