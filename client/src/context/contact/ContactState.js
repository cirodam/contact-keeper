import React, {useReducer} from 'react';
import ContactContext from './contactContext';
import contactReducer from './contactReducer';
import axios from 'axios';
import {
    ADD_CONTACT,
    DELETE_CONTACT,
    SET_CURRENT,
    CLEAR_CURRENT,
    UPDATE_CONTACT,
    FILTER_CONTACTS,
    CLEAR_FILTER, 
    CONTACT_ERROR,
    GET_CONTACTS,
    CLEAR_CONTACTS
} from '../types';

const ContactState = props => {
    const initalState = {
        contacts: null,
        current: null, 
        filtered: null, 
        error: null
    };

    const [state, dispatch] = useReducer(contactReducer, initalState);

    const getContacts = async () => {

        try{
            const res = await axios.get('https://secret-escarpment-92420.herokuapp.com/api/contacts');
            dispatch({type: GET_CONTACTS, payload: res.data});
        }catch(err){
            dispatch({type: CONTACT_ERROR, payload: err.response});
        }
    }

    const addContact = async contact => {

        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        }

        try{
            const res = await axios.post('https://secret-escarpment-92420.herokuapp.com/api/contacts', contact, config);
            dispatch({type: ADD_CONTACT, payload: res.data});
        }catch(err){
            dispatch({type: CONTACT_ERROR, payload: err.response});
        }
    }

    const deleteContact = async id => {
        try{
            await axios.delete(`https://secret-escarpment-92420.herokuapp.com/api/contacts/${id}`);
            dispatch({type: DELETE_CONTACT, payload: id});
        }catch(err){
            dispatch({type: CONTACT_ERROR, payload: err.response});
        }
    }

    const clearContacts = () => {
        dispatch({type: CLEAR_CONTACTS});
    }

    const setCurrent = contact => {
        dispatch({type: SET_CURRENT, payload: contact});
    }

    const clearCurrent = () => {
        dispatch({type: CLEAR_CURRENT});
    }

    const updateContact = async contact => {
        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        }

        try{
            const res = await axios.put(`https://secret-escarpment-92420.herokuapp.com/api/contacts/${contact._id}`, contact, config);
            dispatch({type: UPDATE_CONTACT, payload: res.data});
        }catch(err){
            dispatch({type: CONTACT_ERROR, payload: err.response});
        }
    }

    const filterContacts = text => {
        dispatch({type: FILTER_CONTACTS, payload: text});
    }

    const clearFilter = () => {
        dispatch({type: CLEAR_FILTER});
    }

    return (
        <ContactContext.Provider value={{
            contacts: state.contacts, 
            current: state.current,
            filtered: state.filtered,
            error: state.error,
            addContact, 
            deleteContact,
            setCurrent, 
            clearCurrent,
            updateContact,
            filterContacts,
            clearFilter,
            getContacts,
            clearContacts
        }}>
            { props.children }
        </ContactContext.Provider>
    )
}

export default ContactState